# README #

This program uses the bash scripting language. Check #!/bin/bash points to your interpreter.
To run the program. In the directory execute: bash lookup.sh or simply ./lookup.sh

REPOSITORY: https://clem16@bitbucket.org/clem16/ham-radio-call-lookup.git

### What is this repository for? ###

* Does a grep search on a textfile containing ham radio callsigns.
* Version 1.

### How do I get set up? ###

* Download the repository. Execute the script.
* Dependencies: Check you have correct interpreter.

### Who do I talk to? ###

* Repo owner or admin
* clem16@gmail.com
