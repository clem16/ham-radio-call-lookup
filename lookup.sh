#!/bin/bash
# REPO: https://clem16@bitbucket.org/clem16/ham-radio-call-lookup.git
clear
FILE=amateur.txt
CANADACALL=http://apc-cap.ic.gc.ca/datafiles/amateur.zip
if [ -f $FILE ];
then
	clear
	echo "File $FILE, was downloaded on: $(head -n 1 $FILE)"
	echo -n "ENTER CALLSIGN:"
	read call
	grep -i "$call" amateur.txt

else
	echo "File $FILE does not exist, downloading."
	wget $CANADACALL
	unzip amateur.zip
	clear
	echo -n "ENTER CALLSIGN:"
	read call
	clear
	grep -i "$call" amateur.txt
fi
read -p "Cleanup downloaded files? (y/n) " RESP
if [ "$RESP" = "y" ]; then
rm amateur.txt amateur.zip lisezmoi_amat.txt readme_amat.txt
else
  exit
fi
